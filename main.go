package main

import (
	"fmt"

	"./packages/auth"
	"./packages/conf"
	"./packages/conf/models"
	"./packages/router"

	_ "github.com/go-sql-driver/mysql"
)

var env = "dev"

func main() {
	envConf, err := conf.GetEnvConf("./conf.json", env)
	if err != nil {
		panic("An error has accurred while trying to read EnvConf from config.json. Aborting.")
	}

	initPackages(envConf)

	err = auth.GetUser("root", "root")
	if err != nil {
		fmt.Println(err)
	}

	router.StartRouter()
}

func initPackages(conf confmodels.EnvConf) {
	router.LoadConf(conf)
}
