package conf

import (
	"encoding/json"
	"io/ioutil"

	"./models"
)

// GetConf returns the config set in the config.json file
func GetConf(pathToConf string) (confmodels.Conf, error) {
	confContents, _ := getConfContents(pathToConf)

	var conf confmodels.Conf

	err := json.Unmarshal(confContents, &conf)

	return conf, err
}

// GetEnvConf return the configuration for an environment
func GetEnvConf(pathToConf string, environment string) (confmodels.EnvConf, error) {
	c, err := GetConf(pathToConf)

	var output confmodels.EnvConf
	for i := range c.Envs {
		if c.Envs[i].Env == environment {
			output = c.Envs[i]
			break
		}
	}

	return output, err
}

// getConfContents reads the config file and returns the contents of it
func getConfContents(pathToConf string) ([]byte, error) {
	confString, err := ioutil.ReadFile(pathToConf)

	return confString, err
}
