package confmodels

// EnvConf Config for an environment
type EnvConf struct {
	Env   string `json:"env"`
	Start string `json:"start"`
	Db    string `json:"db"`
	Log   string `json:"log"`
	Port  string `json:"port"`
}
