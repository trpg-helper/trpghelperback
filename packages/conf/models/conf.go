package confmodels

// Conf Config to be used on each environment
type Conf struct {
	Envs []EnvConf `json:"envs"`
}
