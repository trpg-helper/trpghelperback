package conf

import (
	"testing"
)

func TestGetConfContents(t *testing.T) {
	_, err := getConfContents("../../tests/conf/data/conf.json")
	if err != nil {
		t.Error("An error occurred while trying to read data from the file")
	}
}

// TestGetConf tests for config.GetConf
func TestGetConf(t *testing.T) {
	conf, err := GetConf("../../tests/conf/data/conf.json")
	if err != nil {
		t.Error("An error occurred while trying to read data from the file")
	}

	if len(conf.Envs) != 3 {
		t.Errorf("Total number of environments was incorrect, got: %d, want: %d", len(conf.Envs), 3)
	}

	if len(conf.Envs) > 0 {
		if conf.Envs[0].Env != "dev" {
			t.Errorf("Env for EnvConf was incorrect, got: %s, want: %s", conf.Envs[0].Env, "dev")
		}

		if conf.Envs[0].Start != "1" {
			t.Errorf("Start for EnvConf was incorrect, got: %s, want: %s", conf.Envs[0].Start, "1")
		}

		if conf.Envs[0].Db != "2" {
			t.Errorf("Db for EnvConf was incorrect, got: %s, want: %s", conf.Envs[0].Db, "2")
		}

		if conf.Envs[0].Log != "3" {
			t.Errorf("Log for EnvConf was incorrect, got: %s, want: %s", conf.Envs[0].Log, "3")
		}

		if conf.Envs[0].Port != "3000" {
			t.Errorf("Port for EnvConf was incorrect, got: %s, want: %s", conf.Envs[0].Port, "3000")
		}
	}
}

// TestGetConf tests for config.GetConf
func TestGetEnvConf(t *testing.T) {
	conf, err := GetEnvConf("../../tests/conf/data/conf.json", "dev")
	if err != nil {
		t.Error("An error occurred while trying to read data from the file")
	}

	if conf.Env != "dev" {
		t.Errorf("Env for EnvConf was incorrect, got: %s, want: %s", conf.Env, "dev")
	}

	if conf.Start != "1" {
		t.Errorf("Start for EnvConf was incorrect, got: %s, want: %s", conf.Start, "1")
	}

	if conf.Db != "2" {
		t.Errorf("Db for EnvConf was incorrect, got: %s, want: %s", conf.Db, "2")
	}

	if conf.Log != "3" {
		t.Errorf("Log for EnvConf was incorrect, got: %s, want: %s", conf.Log, "3")
	}

	if conf.Port != "3000" {
		t.Errorf("Port for EnvConf was incorrect, got: %s, want: %s", conf.Port, "3000")
	}
}
