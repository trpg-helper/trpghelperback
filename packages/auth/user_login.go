package auth

import (
	"database/sql"

	"../crypt"
	"golang.org/x/crypto/bcrypt"
)

// AddUser adds a user to the database
func AddUser(username string, password string) error {
	// TODO: return user struct

	db, err := sql.Open("mysql", "root:root@(127.0.0.1:3306)/trpg_helper")
	defer db.Close()
	if err != nil {
		return err
	}

	hashedAndSaltedPassword, err := crypt.HashAndSalt([]byte(password))
	if err != nil {
		return err
	}

	stmt, err := db.Prepare("INSERT INTO users VALUES(null, ?, ?)")
	if err != nil {
		return err
	}

	insert, err := stmt.Exec(username, string(hashedAndSaltedPassword))
	if err != nil {
		return err
	}

	id, err := insert.LastInsertId()
	if err != nil {
		return err
	}

	println(id)

	return err
}

// GetUser gets a user from the database
func GetUser(username string, password string) error {
	// TODO: return user struct

	db, err := sql.Open("mysql", "root:root@(127.0.0.1:3306)/trpg_helper")
	defer db.Close()
	if err != nil {
		return err
	}

	stmt, err := db.Prepare("SELECT * FROM users WHERE username=?")
	if err != nil {
		return err
	}

	var id uint32
	var user, pass string

	err = stmt.QueryRow(username).Scan(&id, &user, &pass)
	if err != nil {
		return err
	}

	err = bcrypt.CompareHashAndPassword([]byte(pass), []byte(password))
	if err != nil {
		return err
	}
	println(id, user, pass)

	return err
}
