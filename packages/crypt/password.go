package crypt

import "golang.org/x/crypto/bcrypt"

// HashAndSalt hashes and salt a password
func HashAndSalt(pwd []byte) (string, error) {
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)

	return string(hash), err
}

// ComparePasswords checks if a password is equivalent to a hash
func ComparePasswords(hashedPassword string, plainPassword []byte) (bool, error) {
	byteHash := []byte(hashedPassword)

	err := bcrypt.CompareHashAndPassword(byteHash, plainPassword)
	if err != nil {
		return false, err
	}

	return true, err
}
