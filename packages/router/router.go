package router

import (
	"net/http"
	"os"

	"../auth"
	"../conf/models"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

var conf confmodels.EnvConf

func LoadConf(envConf confmodels.EnvConf) {
	conf = envConf
}

// StartRouter listens to connections
func StartRouter() {
	r := mux.NewRouter()

	r.Handle("/", testHandler).Methods("GET")
	r.Handle("/get-token", auth.GetTokenHandler).Methods("POST")
	r.Handle("/test-token", auth.JwtMiddleware.Handler(testHandler)).Methods("GET")

	http.ListenAndServe(":"+conf.Port, handlers.LoggingHandler(os.Stdout, r))
}

// TestHandler tests
var testHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Ay LMAO"))
})
